import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { registerElement } from 'nativescript-angular/element-registry';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { AppComponent } from './app.component';
import { TranslateModule } from '@ngx-translate/core';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { HttpConfigInterceptor } from './common/helpers/api/providers/http/http.interceptor';
import { UserService } from './services/user.service';

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
  ],
  imports: [
    AppRoutingModule,
    NativeScriptModule,
    NativeScriptCommonModule,
    NativeScriptHttpClientModule,
    NativeScriptUISideDrawerModule,
    HttpClientModule,
    NativeScriptFormsModule,
    TranslateModule.forRoot()
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}

