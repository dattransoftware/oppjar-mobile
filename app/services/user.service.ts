import { Injectable } from "@angular/core";
import { User } from "../classes/user.class";
import { HttpClient } from '@angular/common/http';
import { axios } from '../common/helpers/api';

@Injectable()
export class UserService {

    private apiUrl = "http://textsdaily.success-ss.com.vn:8802/test";

    constructor(private http: HttpClient) { }

    register(user: User) {
        return true;
    }

    login() {
        return new Promise(async(resolve, reject) => {
            const message = await this.getDataFromApi();
            if (message) {
                resolve(message);
            }
        });
    }

    logout() {
        return true;
    }

    resetPassword(email) {
        return true;
    }

    handleErrors(error: any) {
        console.error(error.message);
        return Promise.reject(error.message);
    }

    getDataFromApi() {
        return axios.get(this.apiUrl).then(response => {
            return response;
        });
    }
}
