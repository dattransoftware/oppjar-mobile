import { Component } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import enLang from './common/configs/locale/en';
import esLang from './common/configs/locale/es';

@Component({
  selector: 'oj-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public currentLanguage = 'en';

  constructor(
    private translate: TranslateService
  ) {
    // Add translations
    translate.setTranslation('en', enLang);
    translate.setTranslation('es', esLang);

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');

    //
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLanguage = event.lang;
    });
  }

  ngOnInit(): void {
  }
}
