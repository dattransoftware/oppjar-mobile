import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { Routes } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
  { path: "", redirectTo: "/landing-page", pathMatch: "full" },
  { path: 'landing-page', component: LandingPageComponent},
  { path: "login", loadChildren: () => import("./components/login/login.module").then(m => m.LoginModule) }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
