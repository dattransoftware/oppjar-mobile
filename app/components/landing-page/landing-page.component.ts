/**
 * @author Dat Tran
 * @license MIT License Copyright (c) 2020
 */

import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from 'tns-core-modules/ui/page';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'oj-landing-page',
  templateUrl: './landing-page.component.html'
})
export class LandingPageComponent implements OnInit {

  constructor(
    private routerExtensions: RouterExtensions,
    private _page: Page,
    private userService: UserService
  ) {}

  ngOnInit() {
    this._page.actionBarHidden = true;
  }

  signIn() {
    this.routerExtensions.navigate(["/login"], {
      animated: true,
      transition: {
        name: 'slideLeft',
        duration: 300,
        curve: 'linear'
      }
    });
  }

  signUp() {
    this.userService.login().then((res: any) => {
      console.log(res.data);
    }).catch(err => {
      console.log(err);
    });
  }
}
