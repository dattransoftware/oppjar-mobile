export const env = {
    PRODUCTION: true,
    NODE_ENV: 'production',
    LANGUAGE: {
        DEFAULT: 'en',
        DIRECTORY: '/assets/i18n/',
        EXTENSION: '.json'
    }
};