import { enableProdMode } from '@angular/core';
import { platformNativeScriptDynamic } from 'nativescript-angular/platform';
import { AppModule } from './app.module';
import { env } from './app.config';

if (env.PRODUCTION) {
    enableProdMode();
}

platformNativeScriptDynamic({
 }).bootstrapModule(AppModule);