/**
* @author Dat Tran
* @license MIT License Copyright (c) 2020
*/

export default {
    // General
    TRANSACTIONS: 'Transacciones',
    CARDS: 'Tarjetas',
    MENU: 'Menú',

    sidedrawer: {
        balance: 'Saldo',
        english: 'English',
        spanish: 'Español',
        madeBy: 'Hecho con ❤️ por ',
    },

    balance: {
        income: 'INGRESOS',
        expenses: 'GASTOS',
        balance: 'SALDO',
    },

    cards: {
        title: 'Tarjetas'
    },

    categories: {
        home: 'Hogar',
        transport: 'Transporte',
        communication: 'Comunicación',
        hotel: 'Hotel',
        restaurant: 'Restaurante'
    },

    detail: {
        status: {
            completed: 'Completado',
            pending: 'Pendiente',
        }
    }
}