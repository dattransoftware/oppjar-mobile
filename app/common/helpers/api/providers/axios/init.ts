import axios from 'axios';
import { BASE_API_URL } from '../../../../../common/configs/api';

const axiosInstance = axios.create({
    baseURL: BASE_API_URL,
    headers: {},
    withCredentials: true
});

export default axiosInstance;