import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import Storage from '../../../storage/local.storage';
import { RESPONSE_CODE } from '../../../../constants/response.code';

@Injectable() export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(private router: Router ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const currentUser = Storage.get('user');
        const token = Storage.get('token');

        if (currentUser && currentUser.accessToken) {
            request = request.clone({
                headers: request.headers.set(`Authorization`, token)
            });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({
                headers: request.headers.set('Content-Type', 'application/json')
            });
        }

        request = request.clone({
            headers: request.headers.set('Accept', 'application/json')
        });

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (error.status === RESPONSE_CODE.UNAUTHORIZED) {
                    Storage.set('token', '');
                    Storage.set('user', '');
                    console.log('Your session has timed out. Please log in again.');
                    return;
                }

                return throwError(error);
            })
        );
    }
}
