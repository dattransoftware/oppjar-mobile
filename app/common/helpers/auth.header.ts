import Storage from '../../common/helpers/storage/local.storage';

export const authHeader = () => {
	// return authorization header with jwt token
	let token = JSON.parse(Storage.get('token'));

	if (token) {
		return {
			'Authorization': `${token}`,
			// Cookie: `jwt=${token}`
		};
	}

	return {};
};

