import * as connectivity from "tns-core-modules/connectivity";
import * as Toast from "nativescript-toast";
import { LoadingIndicator } from '@nstudio/nativescript-loading-indicator';

const loadingIndicator = new LoadingIndicator();
const loadingIndicatorOptions = {
    // dimBackground: true,
    // progress: 0.65,
};

export function checkInternetConnection() {
    let connectionType = connectivity.getConnectionType();
    if (connectionType === connectivity.connectionType.none) {
        return false;
    }
    return true;
}

export function showToast(message: string, longTime?: boolean) {
    if (longTime) {
        Toast.makeText(message, Toast.duration.long[0]);
    }
    else {
        Toast.makeText(message).show();
    }
}

export function showLoadingIndicator(message?: string) {
    loadingIndicator.show({
        message: message,
        ...loadingIndicatorOptions
    });
}

export function hideLoadingIndicator() {
    loadingIndicator.hide();
}

