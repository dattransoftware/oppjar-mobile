import { FormGroup } from "@angular/forms";

export const confirmPassword = (password: String, confirmPassword: String) => {
    return (group: FormGroup) => {
        let passInput = group.controls[`${password}`],
            passConfirmInput = group.controls[`${confirmPassword}`];

        if (passInput.value !== passConfirmInput.value) {
            return passConfirmInput.setErrors({ notEquivalent: true });
        } else {
            return passConfirmInput.setErrors(null);
        }
    }
};
